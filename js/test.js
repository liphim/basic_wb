//swiper
var swiper = new Swiper(".Home", {
    spaceBetween:30,
    centeredSlides:true,
    autoplay:{
 delay:2500,
 disableOneInteraction: false
 },
 pagination:{
 el:".swiper-pagination",
 clickable:true,
 },
     navigation: {
       nextEl: ".swiper-button-next",
       prevEl: ".swiper-button-prev",
     },
   });